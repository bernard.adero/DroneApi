# DroneAPI

REST API that allows clients to communicate with the drones.

## Getting started

1. Clone DroneAPI solution to your local machine using git
2. Open downloaded project and run DroneAPI.sln (This will open the solution in installed Visual studio in this case i have used visual studio 2022)
3. Build the project.
4. Run the project using visual studio
5. Navigate to:  https://localhost:7176/swagger/index.html for testing using swagger

## Predifined Parameters
1. Drone state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING)
2. Drone model (Lightweight, Middleweight, Cruiserweight, Heavyweight);

## Tsting required endpoits

1. **To register Drone end point:** DronesControllerV2 => POST/api/DronesControllerV2/create
2. **Add medications:** Medication => POST/api/Medication/create
3. **Check available drones for loading** DronesControllerV2 => GET/api/DronesControllerV2/checkavailabledroneforloading
4. **Load medications to Drone:** LoadMedicine =>POST/api/LoadMedicine/create
5. **View loaded medications per drone:** LoadMedicine=> GET/api/LoadMedicine/{id}
5. **Check drone battery level by droneId:** CheckDroneBattery=> GET/api/CheckDroneBattery/{id}


## Additional end points:
1. **To View all registred Drones:** GET/api/DronesControllerV2/getalldrones
2. **To View all registred medications:** GET/api/Medication/getalllmedications

## Instructions to register drone
1. remember all drones must have model and state which are predefined above.
you must there for enter either **IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING** in state
and **Lightweight, Middleweight, Cruiserweight, Heavyweight** for model to successfully register drone

## Instructions to load drone with mediation
1.First you must register drone successfully
2.Second you must add medications successfully
 since it takes droneId and MedicationId

## Test and Deploy

Use the built-in continuous integration in GitLab.
***

# How I worked through my commits.
1. Started by creating empty new .NET CORE API solution
2. Added my Model classes.
3. Added Enttity framwork classes via nuget
4. Included Entity framwork in my project
5. Created context classes
6. Run dbMigration to create database
7. enhanced the solution by adding interfaces.
8. Ensured the project is buildable and can run

## Support
Help Doccumentaions available on next update.

## Roadmap


## Contributing

## Authors and acknowledgment
Adero Bernard for Musala Interview

## License


